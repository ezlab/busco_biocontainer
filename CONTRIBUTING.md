If you want to implement an improvement, please create an issue on https://gitlab.com/ezlab/busco/issues describing your goal.

A merge request can then be created upon agreement and we will push an updated container on dockerhub.