Source of the BUSCO container https://hub.docker.com/r/ezlabgva/busco/tags

See https://gitlab.com/ezlab/busco_biocontainer/-/blob/master/CONTRIBUTING.md

and also

https://busco.ezlab.org/

https://gitlab.com/ezlab/busco/-/blob/master/README.md

https://gitlab.com/ezlab/busco/-/blob/master/LICENSE

https://gitlab.com/ezlab/busco/issues
